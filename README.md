# README #

This is a sass project for an edu site 
### Building  ###

Requirements: 
Ruby/Compass, semantic-ui-sass

Just run compass-compile against the repo folder

```
#!Terminal

$ compass compile compass-edu
```